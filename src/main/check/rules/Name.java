package main.check.rules;

import main.check.Result;
import main.check.IRuleCheck;
import main.check.RuleResult;
import main.domain.Person;

public class Name implements IRuleCheck<Person>{

	public Result checkRule(Person entity) {
		
		if(entity.getFirstName() == null) {
			return new Result("Brak imienia.", RuleResult.ERROR);
		}
		
		if(entity.getFirstName().equals("")) {
			return new Result("Brak imienia.", RuleResult.ERROR);
		}
		
		
		return new Result("", RuleResult.OK);
	}

}