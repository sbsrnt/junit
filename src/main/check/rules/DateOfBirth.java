package main.check.rules;

import main.check.Result;
import main.check.IRuleCheck;
import main.check.RuleResult;
import main.domain.Person;

public class DateOfBirth implements IRuleCheck<Person> {

	public Result checkRule(Person entity) {
		
		if(entity.getDateOfBirth() == null) {
			return new Result("Błąd przy wpisaniu daty urodzenia.", RuleResult.ERROR);
		}
		
		if(entity.getDateOfBirth().equals("")) {
			return new Result("Błąd przy wpisaniu daty urodzenia.", RuleResult.ERROR);
		}
		
		return new Result("", RuleResult.OK);
	}

}