package main.check.rules;

import main.check.Result;
import main.check.IRuleCheck;
import main.check.RuleResult;
import main.domain.Person;

public class Login implements IRuleCheck<Person> {

	public Result checkRule(Person entity) {
		
		if(entity.getUser().getLogin() == null) {
			return new Result("Niepoprawny format loginu.", RuleResult.ERROR);
		}
		
		if(entity.getUser().getLogin().equals("")) {
			return new Result("Niepoprawny format loginu.", RuleResult.ERROR);
		}	
		return new Result("", RuleResult.OK);
	}

	
	
	
}