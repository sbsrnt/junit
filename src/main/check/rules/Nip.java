package main.check.rules;


import main.check.Result;
import main.check.IRuleCheck;
import main.check.RuleResult;
import main.domain.Person;

public class Nip implements IRuleCheck<Person> {

	public Result checkRule(Person entity) {
		
		if(entity.getNip() == null) {
			return new Result("Brak NIP.", RuleResult.ERROR);
		}
		
		if(entity.getNip().equals("")) {
			return new Result("Brak NIP.", RuleResult.ERROR);
		}
		
			
		if(entity.getNip().length() != 10) {
			return new Result("Niepoprawny format NIP.", RuleResult.ERROR);
		}
		
		
		if(!checkNipControlSum(entity)) {
			return new Result("Bledny numer NIP.", RuleResult.ERROR);
		}
		
	
		return new Result("", RuleResult.OK);
	}
	
	
	
	private boolean checkNipControlSum(Person entity) {
		
		int ScaleTab[] = {6, 5, 7, 2, 3, 4, 5, 6, 7};
		int sum = 0;
		
		for(int i = 0; i < ScaleTab.length; i++) {
			sum += Integer.parseInt(entity.getNip().substring(i, i + 1)) * ScaleTab[i];
		}
		
		if(!entity.getNip().substring(9).equals(Integer.toString(sum % 11))) {	
			return false;		
		}
	
		return true;	
	}

}