package main.check.rules;
import main.check.Result;
import main.check.IRuleCheck;
import main.check.RuleResult;
import main.domain.Person;

public class Email implements IRuleCheck<Person> {

	public Result checkRule(Person entity) {
		
		if(entity.getEmail() == null) {
			return new Result("Brak email.", RuleResult.ERROR);
		}
		
		if(entity.getEmail().equals("")) {
			return new Result("Brak email.", RuleResult.ERROR);
		}

		return new Result("", RuleResult.OK);
	}

}