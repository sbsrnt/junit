package main.check.rules;
import main.check.Result;
import main.check.IRuleCheck;
import main.check.RuleResult;
import main.domain.Person;

public class Password implements IRuleCheck<Person> {

	public Result checkRule(Person entity) {
		
		if(entity.getUser().getPassword() == null) {
			return new Result("Brak hasla.", RuleResult.ERROR);
		}
		
		if(entity.getUser().getPassword().equals("")) {
			return new Result("Brak hasla.", RuleResult.ERROR);
		}
		
		if(entity.getUser().getPassword().length() < 5) {
			return new Result("Haslo jest za krotkie", RuleResult.ERROR);
		}
		
		if(entity.getUser().getPassword().equals(entity.getUser().getPassword().toLowerCase())) {
			return new Result("Haslo musi zawierac conajmniej jedna duza litere.", RuleResult.ERROR);
		}
		
		return new Result("", RuleResult.OK);
	}

}