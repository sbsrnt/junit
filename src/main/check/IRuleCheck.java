package main.check;

public interface IRuleCheck <TEntity> {

	Result checkRule(TEntity entity);
	
}