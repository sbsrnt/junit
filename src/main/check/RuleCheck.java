package main.check;

import java.util.ArrayList;
import java.util.List;

public class RuleCheck<TEntity> {
	
	private List<IRuleCheck<TEntity>> rules;
	
	public RuleCheck() {
		rules = new ArrayList<IRuleCheck<TEntity>>();
	}
	
	public List<IRuleCheck<TEntity>> getRules() {
		return rules;
	}

	public void setRules(List<IRuleCheck<TEntity>> rules) {
		this.rules = rules;
	}

	public List<Result> check(TEntity entity) {
		
		List<Result> result  = new ArrayList<Result>();
		
		for(IRuleCheck<TEntity> rule: rules){
			result.add(rule.checkRule(entity));
		}
		return result;
	}
}
	
