package main.domain;

import java.util.ArrayList;
import java.util.List;

public class User extends Entity {
	
	private String login;
	private String password;
	private List<Role> userRoles;
	
	public User() {
			setUserRoles(new ArrayList<Role>());
	}
		
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public List<Role> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(List<Role> userRoles) {
		this.userRoles = userRoles;
	}
	
	public void addRole(Role role) {
		userRoles.add(role);
	}

}