package main.domain;

public enum EntityState {
	NEW, MODIFIED, UNCHANGED, DELETED, UNKNOWN;
}