package main.UoW;

import main.domain.Entity;

public interface UnitOfWorkRepository {
	
	void persistAdd(Entity entity);
    void persistRemove(Entity entity);
    void persistUpdate(Entity entity);

}