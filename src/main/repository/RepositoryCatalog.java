package main.repository;

import main.domain.EnumerationValue;
import main.domain.User;

public interface RepositoryCatalog {
	
	public Repository<EnumerationValue> enumerations();
	public Repository<User> users();

}