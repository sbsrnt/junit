package main.repository;

import main.domain.EnumerationValue;

public interface EnumerationValueRepository extends Repository<EnumerationValue> {
	
	EnumerationValue withName(String name);
    EnumerationValue withKey(int key, String name);
    EnumerationValue withStringKey(String key, String name);

}