package main.repository;

import java.util.ArrayList;
import java.util.List;

import main.domain.EnumerationValue;
import main.domain.RolesPermission;
import main.domain.User;
import main.domain.Role;

public class Database {

	List<User> users;
	List<RolesPermission> userPermission;
	List<Role> userRoles;
	List<EnumerationValue> enumValues;
	
	
	public Database() {
		
		users = new ArrayList<User>();
		userPermission = new ArrayList<RolesPermission>();
		userRoles = new ArrayList<Role>();
		enumValues = new ArrayList<EnumerationValue>();
		
	}
	
	
}