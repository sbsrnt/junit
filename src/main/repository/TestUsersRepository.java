package main.repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import main.UoW.UnitOfWorkRepository;
import main.domain.Entity;
import main.domain.EntityState;
import main.domain.RolesPermission;
import main.domain.User;
import main.domain.Role;

public class TestUsersRepository implements UserRepository, UnitOfWorkRepository {
	
	protected PreparedStatement insert;
	protected PreparedStatement delete;
	protected PreparedStatement update;
	
	Database db = new Database();


	
	protected void setUpUpdateQuery(User entity) throws SQLException { 
	}
	protected void setUpInsertQuery(User entity) throws SQLException { 
	}
	
	
	public User withId(int id) {
		User user = null;
		
		for(User v: db.users)
		{
			if (v.getId() == id) {
				user = v;
			}
		}
		return user;
	}
	public void add(User entity) {
		entity.setState(EntityState.NEW);
		db.users.add(entity);
		
	}
	public void delete(User entity) {
		entity.setState(EntityState.DELETED);
		db.users.remove(entity);
		
	}
	public void modify(User entity) {
		entity.setState(EntityState.MODIFIED);
		@SuppressWarnings("unused")
		User user = null;
		
		for(User v: db.users)
		{
			if(v.getId() == entity.getId()) {
				user = v;
			}
		}
	}
	public int count() {
		return db.users.size();
	}
	public void persistAdd(Entity entity) {

	}
	public void persistRemove(Entity entity) {

	}
	public void persistUpdate(Entity entity) {

	}
	public User withLogin(String login) {
		User user = null;
		
		for(User v: db.users)
		{
			if (v.getLogin() == login) {
				user = v;
			}
		}
		return user;
	}
	public User withLoginAndPassword(String login, String password) {
		User user = null;
		
		for(User v: db.users)
		{
			if (v.getLogin() == login && v.getPassword() == password) {
				user = v;
			}
		}
		return user;
	}
	public void setupPermission(User user) {
		Role role = new Role();
		RolesPermission rolesPermission = new RolesPermission();
		rolesPermission.setPermissionId(1);
		rolesPermission.setRoleId(1);
		role.addRolePermission(rolesPermission);
		role.addUser(user);
		role.setUserId(0);
		role.setRoleId(rolesPermission.getRoleId());
		user.addRole(role);
		rolesPermission.addRole(role);
		
	}

}