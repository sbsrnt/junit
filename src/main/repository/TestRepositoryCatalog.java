package main.repository;

import main.domain.EnumerationValue;
import main.domain.User;

public class TestRepositoryCatalog implements RepositoryCatalog {

	
	private Database db = new Database();
	
	public Repository<EnumerationValue> enumerations() {
		return (EnumerationValueRepository) db.enumValues;
	}

	public Repository<User> users() {
		return (UserRepository) db.users;
	}

}