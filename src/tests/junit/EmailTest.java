package tests.junit;

import static org.junit.Assert.*;
import main.check.Result;
import main.check.RuleResult;
import main.check.rules.Email;
import main.domain.Person;

import org.junit.Test;


public class EmailTest {

	Email rule = new Email();
	
	@Test
	public void check_email_is_correct(){
		Person p = new Person();
		p.setEmail("abc@abc.com");
		Result result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.OK));
		
	}
	
	@Test
	public void check_email_is_not_correct(){
		Person p = new Person();
		p.setPesel("11111111111");
		Result result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.ERROR));
		
	}

}