package tests.junit;

import static org.junit.Assert.*;

import org.junit.Test;

import main.check.Result;
import main.check.RuleResult;
import main.check.rules.Password;
import main.domain.Person;
import main.domain.User;

public class PasswordTest {

	Password rule = new Password();
	
	@Test
	public void password_strong(){
		Person p = new Person();
		User u = new User();
		u.setPassword("qwerty.123456");
		p.setUser(u);
		Result result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.OK));
		
	}
	
	@Test
	public void password_weak(){
		Person p = new Person();
		User u = new User();
		u.setPassword("pass");
		p.setUser(u);
		Result result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.ERROR));
		
	}

}