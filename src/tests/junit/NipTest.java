package tests.junit;

import static org.junit.Assert.*;

import org.junit.Test;

import main.check.Result;
import main.check.RuleResult;
import main.check.rules.Nip;
import main.domain.Person;

public class NipTest {

	Nip rule = new Nip();
	
	@Test
	public void check_nip_is_correct(){
		Person p = new Person();
		p.setNip("3623981230");
		Result result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.OK));
		
	}
	
	@Test
	public void check_nip_is_not_correct(){
		Person p = new Person();
		p.setNip("11111111111");
		Result result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.ERROR));
		
	}
}