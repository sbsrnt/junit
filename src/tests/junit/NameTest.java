package tests.junit;

import static org.junit.Assert.*;

import org.junit.Test;

import main.check.Result;
import main.check.RuleResult;
import main.check.rules.Name;
import main.domain.Person;

public class NameTest {

	Name rule = new Name();
	
	@Test
	public void name_null(){
		Person p = new Person();
		Result result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.ERROR));	
	}
	
	@Test
	public void name_empty(){
		Person p = new Person();
		p.setFirstName("");
		Result result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.ERROR));
		
	}
	
	@Test
	public void return_name_notnull(){
		Person p = new Person();
		p.setFirstName("imie");
		Result result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.OK));
		
	}
	
	

}